import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap-css-only';
import App from './components/App';

ReactDOM.render(<App/>, document.getElementById('app'));
