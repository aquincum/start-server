import React from 'react';
import SoundFile from './SoundFile';
import {Table} from 'react-bootstrap';

class SoundList extends React.Component {
    render(){
        const entryLines = this.props.entries.map((entry, ix) => <SoundFile data={entry} key={ix}></SoundFile>);
        
        return(
            <Table striped>
                <thead><tr>
                    <th>username</th>
                    <th>device ID</th>
                    <th>session ID</th>
                    <th>audio</th>
                    <th>lpc</th>
                    <th>ratings</th>
                    <th></th>
                </tr></thead>
                <tbody>
                    {entryLines}
                </tbody>
            </Table>
        )
    }
    
}

export default SoundList;
