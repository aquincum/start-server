import React from 'react';
import Downloader from "./Downloader"
//import {Label, Table} from 'react-bootstrap';
import NoFileUploaded from "./NoFileUploaded"
import AudioStreamer from "./AudioStreamer"

class SoundFile extends React.Component {
    render(){
        var username = this.props.data.metadata ? this.props.data.metadata.username : "unspecified";
        var deviceID = this.props.data.metadata ? this.props.data.metadata.deviceID : "unspecified";
        var audio = this.props.data.audioDataID ?
                    <Downloader session={this.props.data.session_id} type="audio"></Downloader> :
                    <NoFileUploaded type="audio" />
        var lpc = this.props.data.lpcDataID ?
                  <Downloader session={this.props.data.session_id} type="lpc"></Downloader> :
                    <NoFileUploaded type="lpc" />
        var ratings = this.props.data.ratingsID ?
                      <Downloader session={this.props.data.session_id} type="ratings"></Downloader> :
                    <NoFileUploaded type="ratings" />
        return(
                <tr>
                  <td>{username}</td>
                  <td className="col-lg-2 col-xs-2">{deviceID}</td>
                  <td>{this.props.data.session_id}</td>
                  <td>{audio}</td>
                  <td>{lpc}</td>
                  <td>{ratings}</td>
                </tr>
        )
    }
}

/* Extend this with shape */
SoundFile.propTypes = {
    data: React.PropTypes.object
};

export default SoundFile;
