import React from 'react';
import {Button,Glyphicon} from 'react-bootstrap'

class NoFileUploaded extends React.Component {
    render(){
        return (
            <Button bsStyle='warning'>No file <Glyphicon glyph="remove" ></Glyphicon></Button>
            )
    }

}

NoFileUploaded.propTypes = {
    type: React.PropTypes.string
}

export default NoFileUploaded;
