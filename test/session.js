var assert = require("assert");
var request = require("supertest");
var fs = require("fs");
var mongo = require("mongodb");
var moment = require("moment");
var server;
var audioFN = "test/ttt-2015-12-08T17-22-52-audio.caf";
var lpcFN = "test/ttt-2015-12-08T17-22-52-lpc.csv";
var metaFN = "test/ttt-2015-12-08T17-22-52-meta.csv";
var ratingsFN = "test/82A35821-51C1-4B88-B357-FED0944F198A-2016-03-03T11-44-09-ratings.csv";
var audioData;
var audioAltData;
var lpcData;
var metaDataData;
var metaDataFields;
var my_id;

before("Reading test files", function(){
    rawAudioData = fs.readFileSync(audioFN, "binary");
    audioData = fs.readFileSync("test/ttt-2015-12-08T17-22-52-audio.flac");
    audioAltData = fs.readFileSync("test/ttt-2015-12-08T17-22-52-audio-alt.flac");
    lpcData = fs.readFileSync(lpcFN);
    ratingsData = fs.readFileSync(ratingsFN);
    metaDataData = fs.readFileSync(metaFN);
    metaDataFields = metaDataData.toString().split("\n")[1].split(", ");
    metaDataFields[0] = parseInt(metaDataFields[0].replace(/[^0-9\.]/,""));
    metaDataFields[3] = moment(metaDataFields[3],"YYYY-MM-DDTHH-mm-ss").toDate();
});

before("Connecting to the server", function(){
    server = require("../index");
});

describe("Getting version", function(){
    it("Should work", function(done){
        request(server)
            .get("/version")
            .expect(200)
            .expect(require("../package.json").version, done);
    });
});

describe("Uploading session in whole [deprecating]", function() {
    it("Should talk to the client", function(done){
        request(server)
            .post("/session?session_id=431")
            .attach("audioData", audioFN)
            .attach("lpcData", lpcFN)
            .attach("metadataData", metaFN)
            .expect(200)
            .expect(function(res){
                assert.ok(JSON.parse(res.text).id);
            }, done)
            .end(done);
    });
    it("Session should be there", function(){
        return mongo.MongoClient.connect(require("../server/database").url)
            .then(function(db){
                return mongo.GridStore.list(db);
            }).then(function(files){
                assert.equal(files.length, 2);
            });
    });
    it("Content should be correct", function(){
        var db;
        return mongo.MongoClient.connect(require("../server/database").url)
            .then(function(_db){
                db = _db;
                return db.collection("sessions").findOne();
            })
            .then(function(doc){
                console.log("D", doc);
                assert.ok(doc.session_id);
                my_id = doc.session_id;
                return Promise.all([
                    mongo.GridStore.read(db, doc.audioDataID),
                    mongo.GridStore.read(db, doc.lpcDataID),
                    Promise.resolve(doc)
                ]);
            })
            .then(function(datas){
                if(datas[0].toString() != audioData.toString()){
                    assert.deepEqual(datas[0].toString().length, audioAltData.toString().length);
                }
                assert.deepEqual(datas[1], lpcData);
                assert.equal(datas[2].metadata.stream_sample_rate, metaDataFields[0]);
                assert.equal(datas[2].metadata.username, metaDataFields[1]);
                assert.equal(datas[2].metadata.deviceID, metaDataFields[2]);
                assert.deepEqual(datas[2].metadata.start_date, metaDataFields[3]);
            });
    });
});


describe("Uploading session piece by piece", function() {
    it("Should upload metadata", function(done){
        request(server)
            .post("/session/simple/metadata?session_id=432")
            .set("fileName", "metafile.csv")
            .send(metaDataData.toString())
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should upload lpc", function(done){
        request(server)
            .post("/session/simple/lpc?session_id=432")
            .set("fileName", "lpc.csv")
            .send(lpcData.toString())
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should upload audio", function(done){
        request(server)
            .post("/session/simple/audio?session_id=432")
            .set("fileName", "audio.caf")
            .send(rawAudioData.toString())
             .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should upload ratings", function(done){
        request(server)
            .post("/session/simple/ratings?session_id=432")
            .set("fileName", "ratings.csv")
            .send(ratingsData.toString())
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Session should be there", function(){
        return mongo.MongoClient.connect(require("../server/database").url)
            .then(function(db){
                return mongo.GridStore.list(db);
            }).then(function(files){
                assert.equal(files.length, 5);
            });
    });
    it("Content should be correct", function(){
        var db;
        return mongo.MongoClient.connect(require("../server/database").url)
            .then(function(_db){
                db = _db;
                return db.collection("sessions").findOne({session_id: "432"});
            })
            .then(function(doc){
                console.log("D", doc);
                assert.ok(doc.session_id);
                return Promise.all([
                    mongo.GridStore.read(db, doc.audioDataID),
                    mongo.GridStore.read(db, doc.lpcDataID),
                    mongo.GridStore.read(db, doc.ratingsID),
                    Promise.resolve(doc)
                ]);
            })
            .then(function(datas){
                if(datas[0].toString() != audioData.toString()){
                    assert.deepEqual(datas[0].toString().length, audioAltData.toString().length);
                }
                assert.deepEqual(datas[1], lpcData);
                assert.deepEqual(datas[2], ratingsData);
                assert.equal(datas[3].metadata.stream_sample_rate, metaDataFields[0]);
                assert.equal(datas[3].metadata.username, metaDataFields[1]);
                assert.equal(datas[3].metadata.deviceID, metaDataFields[2]);
                assert.deepEqual(datas[3].metadata.start_date, metaDataFields[3]);
            });
    });
});


describe("Uploading one by one, using multipart form", function(){
    it("Should work based on metadata test", function(done){
        request(server).post("/session/metadata?session_id=433")
            .field("session_id", "433")
            .attach("file", metaFN)
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should work based on lpc test", function(done){
        request(server).post("/session/lpc?session_id=433")
            .field("session_id", "433")
            .attach("file", lpcFN)
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should work based on audio test", function(done){
        request(server).post("/session/audio?session_id=433")
            .field("session_id", "433")
            .attach("file", audioFN)
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should work based on ratings test", function(done){
        request(server).post("/session/ratings?session_id=433")
            .field("session_id", "433")
            .attach("file", ratingsFN)
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });

});


describe("Getting session", function(){
    it("Metadata should be retrieved unharmed", function(done){
        request(server)
            .get("/session?session_id=" + my_id)
            .expect(200)
            .expect(function(res){
                //console.log(res.body);
                assert.equal(res.body.session_id, my_id);
                assert.equal(res.body.metadata.stream_sample_rate, metaDataFields[0]);
                assert.equal(res.body.metadata.username, metaDataFields[1]);
                assert.equal(res.body.metadata.deviceID, metaDataFields[2]);
                assert.deepEqual(moment(res.body.metadata.start_date).toDate(), metaDataFields[3]);
            })
            .end(done);
    });
    it("Should tell me if session doesn't exist", function(done){
        request(server)
            .get("/session?session_id=4u0985432527435723")
            .expect(404)
            .expect({error: "No such session found"}, done);
    });
    it("Downloaded LPC file should be retrieved unharmed", function(done){
        request(server).get("/session/lpc?session_id=" + my_id)
            .expect(200)
            .expect(function(res){
                assert.equal(res.text, lpcData.toString());
            })
            .end(done);
    });
    it("Downloaded audio file should be retrieved unharmed", function(done){
        request(server).get("/session/audio?session_id=" + my_id)
            .expect(200)
            .expect(function(res){
                if(res.text != audioData.toString()){
                    assert.deepEqual(res.text.length, audioAltData.toString().length);
                }
                else{
                    assert.equal(1, 1);
                }
            })
            .end(done);
    });
    it("Downloaded ratings file should be retrieved unharmed", function(done){
        request(server).get("/session/ratings?session_id=" + (parseInt(my_id,10)+1))
            .expect(200)
            .expect(function(res){
                assert.equal(res.text, ratingsData.toString());
            })
            .end(done);
    });
    it("Should list the session(s)", function(done){
        request(server)
            .get("/session")
            .expect(200)
            .expect(function(res){
                var data = JSON.parse(res.text);
                assert.equal(data.length, 3);
                assert.equal(data[0].metadata.username, metaDataFields[1]);
            })
            .end(done);
    });
});


after("Remove Grid and session", function(){
    var db;
    return mongo.MongoClient.connect(require("../server/database").url)
        .then(function(_db){
            db = _db;
            return db.collection("fs.files").find().toArray();
        }).then(function(names){
            return Promise.all(names.map(function(name){
                return mongo.GridStore.unlink(db, name._id);
            }));
        }).then(function(){
            db.collection("sessions").remove();
        });
});


