var database = require("../database");
var audioConvert = require("../audioConvert");
var Promise = require("bluebird");

/**
 * @param {undefined|String|Object} init if String, that's the session_id, if Object,
 * it's a returned object from mongodb.
 */
var Session = function(init){
    if(typeof init !== "object"){
        this.session_id = init || 0;
        this.metadata = {};
        this.audioDataID = null;
        this.lpcDataID = null;
        this.ratingsID = null
        this._id = null;
    }
    else {
        this._id = init._id;
        this.session_id = init.session_id;
        this.metadata = init;
        this.audioDataID = init.audioDataID;
        this.lpcDataID = init.lpcDataID;
        this.ratingsID = init.ratingsID;
    }
};

/**
 * Inserts a recording session to the database. Pass it an
 * info object and a callback.
 * @param {Object} obj A parameter collection object
 * @param {String} obj.audioDataFilename A file location containing the audio file
 * @param {String} obj.lpcDataFilename A file location containing the lpc file
 * @param {String} obj.metadataDataFilename A file location containing the metadata file
 * @param {String} obj.session_id The session ID
 * @returns {Promise<Session>} The Session
 */
Session.insert = function(obj){
    return audioConvert.toStorage(obj.audioDataFilename).then(function(cfn){
        return Promise.all([
            database.gridSaveFile(database.db(), cfn),
            database.gridSaveFile(database.db(), obj.lpcDataFilename)
        ]);
    }).then(function(objIDs){
        var coll = database.db().collectionP("sessions");
        var doc = {metadata: obj.metadata};
        doc.audioDataID = objIDs[0];
        doc.lpcDataID = objIDs[1];
        doc.session_id = obj.session_id;
        return coll.insertOneAsync(doc);
    }).then(function(r){
        var s =  new Session(r.ops[0]);
        return s;
    });
};

/** Inserts or updates a document in the database. Only updates
 * the given field.
 * @param {String} field Which field to update
 * @returns {Promise<Boolean>} Whether the operation succeeded.
 */
Session.prototype.updateField = function(field){
    var updateQuery = { $set: {} };
    updateQuery.$set[field] = this[field];
    return database
        .db()
        .collectionP("sessions")
        .findOneAndUpdateAsync({
            session_id: this.session_id
        },updateQuery,{
            upsert: true
        }).then(function(r){
            return r.ok === 1;
        });
};

/** Upload the LPC file. Doesn't delete the replaced one
 * since I don't think replacement will ever really be needed!
 * @param {String} path File path
 * @returns {Promise<Boolean>} Whether the operation succeeded.
 */
Session.prototype.uploadLPC = function(path){
    var that = this;
    return database.gridSaveFile(database.db(), path)
        .then(function(objID) {
            that.lpcDataID = objID;
            return that.updateField("lpcDataID");
        });
};

/** Upload the Ratings file. Doesn't delete the replaced one
 * since I don't think replacement will ever really be needed!
 * @param {String} path File path
 * @returns {Promise<Boolean>} Whether the operation succeeded.
 */
Session.prototype.uploadRatings = function(path){
    var that = this;
    return database.gridSaveFile(database.db(), path)
        .then(function(objID){
            that.ratingsID = objID;
            return that.updateField("ratingsID");
        });
};

/** Upload the audiofile. Doesn't delete the replaced one
 * since I don't think replacement will ever really be needed!
 * @param {String} path File path
 * @returns {Promise<Boolean>} Whether the operation succeeded.
 */
Session.prototype.uploadAudio = function(path){
    var that = this;
    return audioConvert
        .toStorage(path)
        .then(function(fn){
            return database.gridSaveFile(database.db(), fn);
        })
        .then(function(objID){
            that.audioDataID = objID;
            return that.updateField("audioDataID");
        });
};

/**
 * Retrieves the session for a given session based on session ID
 * @param {String} id The object ID, as restored in _id for mongo
 * @returns {Promise<Object>} A promise with the session info,
 * consisting of stream_sample_rate, username, deviceID, start_date,
 * etc., audioDataID and lpcDataID returned. If no such session exist, a
 * rejected promise is returned
 */
Session.find = function(id){
    var coll = database.db().collection("sessions");
    return coll.findOne({"session_id": id}).then(function(doc){
        if(!doc) {
            return Promise.reject(404);
        }
        return new Session(doc);
    });
}



/**
 * Retrieves a specific file for a given session
 * @param {String} file Which file? "lpc", "audio" or "ratings"
 * @returns {Promise<stream.Readable>} The file as a stream
 */
Session.prototype.download = function(file){
    var fileID = 0;
    switch(file){
    case "lpc":
        fileID = this.lpcDataID;
        break;
    case "audio":
        fileID = this.audioDataID;
        break;
    case "ratings":
        fileID = this.ratingsID;
        break;
    default:
        return Promise.reject("Incorrect request for Session.prototype.download");
    }

    return database.gridReadFileStream(database.db(), fileID);
}


/**
 * Lists all sounds in the system. Returns metadata. Might not be
 * exposed in a release...
 * @returns {Promise<Object[]>} All the metadata
 */
Session.listAll = function(){
    var coll = database.db().collection("sessions");
    return coll.find().toArray();
}

module.exports = Session;
