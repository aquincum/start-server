var Promise = require("bluebird");
var GridStore = require("mongodb").GridStore;
var ObjectID = module.exports.ObjectID = require("mongodb").ObjectID;

/**
 * Just the database settings go here, the models will deal with the
 * details. Also, wrapping it around Promises.
 * @module
 */



/**
 * Figures out the URL of the database: if we're on Amazon EBS,
 * we're connecting to the linked mongo, otherwise we're running
 * on localhost. And if we have the STARTSERVER_MONGODB_ADDR environment
 * vairable set, use that; with STARTSERVER_MONGODB_PORT or 27017 
 * by default
 * @returns String
 */
var getURL = function(){
    var addr = process.env.STARTSERVER_MONGODB_ADDR || process.env.MONGODB_PORT_27017_TCP_ADDR || "localhost",
        port = process.env.STARTSERVER_MONGODB_PORT || process.env.MONGODB_PORT_27017_TCP_PORT || 27017,
        dbname = "startapp";
    return "mongodb://" + addr + ":" + port + "/" + dbname;
};

/** The URL of the database */
module.exports.url = getURL();
/** The raw unpromisified MongoClient object */
module.exports.MongoClient = require("mongodb").MongoClient;
/** The promisified MongoClient connection */
module.exports.mongoConnect = function(){
    var mc = require("mongodb").MongoClient;
    return mc.connect(module.exports.url)
        .then(function(db){
            console.log("MongoDB connected at " + module.exports.url);
            // Polyfill promisified collection
            db.collectionP = function(coll){
                var c = db.collection(coll);
                Promise.promisifyAll(c);
                return c;
            };
            return db;
        });
};
/** This is the database, with promisified collection request */
var _db = null;
/** Plain setter */
module.exports.setDB = function(db){
    _db = db;
};
/** Plain getter */
module.exports.db = function(){
    return _db;
};

/** 
 * Saves a file from the disk to GridStore.
 * @param db The database
 * @param fileName The file name on the disk to store
 * @returns Promise<ObjectID> A promise that resolves with the unique ID
 * of the file.
 */
module.exports.gridSaveFile = function(db, fileName){
    var fileId = new ObjectID();
    return GridStore.exist(db, fileId).then(function(exists){
        if(exists){
            return Promise.reject("Problem with unique ID.");
        }
    }).then(function(){
        var gridStore = new GridStore(db, fileId, "w");
        return gridStore.open();
    }).then(function(gridStore){
        return gridStore.writeFile(fileName);
    }).then(function(){
        return fileId;
    });
};

/**
 * Reads back a file from GridStore returning a stream.
 * @param db The database
 * @param {ObjectID} fileID The unique ID of the file
 * @returns Promise<stream.Readable> A readable stream
 */
module.exports.gridReadFileStream = function(db, fileId){
    return GridStore.exist(db, fileId).then(function(exists){
        if(!exists){
            return Promise.reject("File does not exist");
        }
    }).then(function(){
        var gridStore = new GridStore(db, fileId, "r");
        return gridStore.open();
    }).then(function(gridStore){
        return gridStore.stream();
    });
};
