var express = require("express");
var logger = require("morgan");
var route = require("./routes");

/**
 * The server itself
 */
var app = express();

app.use(logger("dev"));
route(app);
app.use(express.static("client", {
    extensions: ["html"]
}));

module.exports = app;
